﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeepCameraAtHeight : MonoBehaviour {

	public float _height = 4.95f;
	private GameObject _playerObject;

	// Use this for initialization
	void Start () {
		//	_height = transform.position.y;
		_playerObject = GameObject.FindWithTag("Player");
	}
	
	// Update is called once per frame
	void Update () {
		//to try and keep it at the same height;
		transform.position = new Vector3(_playerObject.transform.position.x, _height, transform.position.z);
	}
}