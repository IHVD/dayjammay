﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KangerooAI : MonoBehaviour {

	public bool attack;
	public bool previousAttack;
	public float _attackInterval;
	public float _attackIntervalHolder;

	public GameObject hittingObjectPlayer;

	public List<Sprite> spriteList;

	private void Start() {
		_attackInterval = _attackIntervalHolder;
	}

	// Update is called once per frame
	void Update () {

		if (ChatSystem.CSystem._chatActive) {
			return;
		}

		_attackInterval -= Time.deltaTime;

		if (_attackInterval <= 0.1f) {
			Attack();
			previousAttack = true;
		}

	}

	void Attack() {
		//If it needs to attack,

		if (attack) { //actually attack.
			transform.GetComponentInChildren<BoxCollider2D>().size = new Vector2(7f, transform.GetComponentInChildren<BoxCollider2D>().size.y);
			transform.GetComponent<SpriteRenderer>().sprite = spriteList[1];
			_attackInterval = _attackIntervalHolder;

			if(hittingObjectPlayer != null) {
				hittingObjectPlayer.GetComponent<Health>().TakeDamage(34f);
			}

			attack = false;
		} else { //reset attack
			transform.GetComponentInChildren<BoxCollider2D>().size = new Vector2(3.66f, transform.GetComponentInChildren<BoxCollider2D>().size.y);
			transform.GetComponent<SpriteRenderer>().sprite = spriteList[0];
			previousAttack = false;
			attack = true;
		}

		previousAttack = false;
		_attackInterval = _attackIntervalHolder;
	}

	private void OnCollisionEnter2D(Collision2D collision) {

		if (collision.collider.tag == "Player") {
			hittingObjectPlayer = collision.collider.gameObject;
		}
	}

	private void OnCollisionExit2D(Collision2D collision) { 

		hittingObjectPlayer = null;
	}
}
