﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneController : MonoBehaviour {

	//public static int currentScene;
	public float planeWaitTimer;

	private void Awake() {
		DontDestroyOnLoad(this.gameObject);
		
	}

	private void OnLevelWasLoaded(int level) {
		if(SceneManager.GetActiveScene().name == ("Outback")) {
			GameObject.FindWithTag("Player").GetComponent<PlayerMovement>()._inAus = true;
			PlayerMovement._hasVegi = false;
		}
	}

	public static void NextScene() {
		if (SceneManager.sceneCount <= SceneManager.GetActiveScene().buildIndex + 1) {
			SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
		} else {
			return;
		}
	}

	public void Update() {

		if (ChatSystem.CSystem._chatActive) {
			return;
		}

		if (SceneManager.GetActiveScene().name == "plane") {
			planeWaitTimer -= Time.deltaTime;

			if (planeWaitTimer <= 0) {
				if (!PlayerMovement._hasVegi) {
					SceneManager.LoadScene("NoVegimiteScene");
					PlayerMovement._hasVegi = false;
				} else {
					NextScene();
				}
			}
		}
	}
}
