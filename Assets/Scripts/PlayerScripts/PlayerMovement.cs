﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {

	public float movementSpeed;
	public float thrust = 50.0f;

	public Rigidbody2D _rb;
	public bool _isGrounded;
	public bool _canJump;

	public bool _left;
	public bool _changedTexture;
    //TODO add canJump bool.
    public static bool _hasVegi;
	public bool _inAus;

	public bool _punching;
	public float _punchInterval;

	public GameObject hittingObject;

	public List<Sprite> spriteList;

	private void Start() {
		_rb = GetComponent<Rigidbody2D>();
		if (_inAus) {
			transform.GetComponentInChildren<SpriteRenderer>().sprite = spriteList[2];
		}
	}


	void Update () {


		//debugging the controller
	//	Debug.Log(Input.GetAxis("ControllerAttack"));

		//Hacky way of assuming that it's on the floor.
		if (transform.position.y < 2.6f) {
			_isGrounded = true;
		}

		if (!ChatSystem.CSystem._chatActive) {
			

			//move around woow
			if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow) || Input.GetAxis("Horizontal") < -0.2f) {
				transform.position +=  movementSpeed * Time.deltaTime * Vector3.left;
				if (!_changedTexture) {
					ChangeTextureDirection(true);
				}

			}

			if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow) || Input.GetAxis("Horizontal") > 0.2f) {
				transform.position += movementSpeed * Time.deltaTime * Vector3.right;
				if (_changedTexture) {
					ChangeTextureDirection(false);
				}
			}

			//primary mouse button
			if (Input.GetKeyDown(KeyCode.Mouse0) || Input.GetAxis("ControllerAttack") >= 0.8f) {
				if (_punchInterval <= 0) {
					_punching = true;
					_punchInterval = 0.2f;
					Punch();
					//Punching, check collision.
					transform.GetComponentInChildren<BoxCollider2D>().size = new Vector2(3f, transform.GetComponentInChildren<BoxCollider2D>().size.y);
					if (!_inAus) {
						transform.GetComponentInChildren<SpriteRenderer>().sprite = spriteList[1];
					} else {
						transform.GetComponentInChildren<SpriteRenderer>().sprite = spriteList[3];
					}

				}
				
			}else if (Input.GetKeyUp(KeyCode.Mouse0) || Input.GetAxis("ControllerAttack") <= 0.5f) {
				EndPunch();
			}

		}//end chatActive check

		//Jumping
		//Since there's already a check in here to see if chat is active, it doesn't have to be in the whole check thing.
		if ((Input.GetKeyDown(KeyCode.Space) || Input.GetButtonDown("Fire1")) && _isGrounded) {
		
			if (ChatSystem.CSystem._chatActive) {
				ChatSystem.CSystem.NextMessage();
				return;
			}

			_rb.AddForce(transform.up * thrust);
			_isGrounded = false;
		}

		if (_punchInterval >= 0) {
			_punchInterval -= Time.deltaTime;
		}


	}

	void ChangeTextureDirection(bool left) {
		if (!_changedTexture && left) {
			
			_changedTexture = true;
			transform.GetComponentInChildren<SpriteRenderer>().flipX = left;
		}

		if (_changedTexture && !left) {
			
			transform.GetComponentInChildren<SpriteRenderer>().flipX = left;
			_changedTexture = false;
		}
	}

	private void OnCollisionEnter2D(Collision2D collision) {
		

		if (collision.collider.tag == "Spider" || collision.collider.tag == "Snake" || collision.collider.tag == "Kangeroo") {
			hittingObject = collision.collider.gameObject;
			
		}else if(collision.collider.tag == "vegemite") {
			_hasVegi = true;
			Destroy(collision.collider.gameObject);
		}
		
	}

	private void OnCollisionExit2D(Collision2D collision) {
		if (collision.collider.tag == "Spider" || collision.collider.tag == "Snake" || collision.collider.tag == "Kangeroo") {
			//it did stuff here
		}
		
		hittingObject = null;
	}

	private void Punch() {
	
		if(_punchInterval >= 0f) {

		if (hittingObject != null) {
			hittingObject.GetComponent<Health>().TakeDamage(50f);
		}
		//_punching = false;
		}
		_punchInterval = 0.2f; // second
		EndPunch();
		
	}

	private void EndPunch() {
		//Ending the punch
		_punching = false;
		transform.GetComponentInChildren<BoxCollider2D>().size = new Vector2(2f, transform.GetComponentInChildren<BoxCollider2D>().size.y);
		if (!_inAus) {
			transform.GetComponentInChildren<SpriteRenderer>().sprite = spriteList[0];
		} else {
			transform.GetComponentInChildren<SpriteRenderer>().sprite = spriteList[2];
		}
	}
}
