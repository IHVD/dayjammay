﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

[System.Serializable]
public class ChatMessage {
	public int _chatTexture;
	public string _chatText;
}

[System.Serializable]
public class ChatMessageList {
	public List<ChatMessage> chatMessages;
}

public class ChatSystem : MonoBehaviour {

	public static ChatSystem CSystem { get; set; }

	public bool _chatActive;

	//1 = Sean
	//2 = Coyote
	//3 = Herman
	public List<Sprite> chatHeadTextures;
	//public List<ChatMessage> chatMessages;

	//public ArrayList chatMessagesList;

	public List<ChatMessageList> chatMessageList;
		
	/*GUI Chat Layout
	* Panel
	*	Texture
	*	Text?
	GUI Chat Layout End */
	public GameObject _chatPanel;
	public GameObject _chatTexture;
	public Text _chatText;

	public int currentCategory;
	public int currentMessage;

	public void Start() {
		CSystem = this;
		//ShowMessage(chatMessageList[0][0]);
		currentCategory = SceneManager.GetActiveScene().buildIndex;
		ShowMessageInt(currentCategory, 0);
		
		//if(currentCategory )
		_chatActive = true;
		//DontDestroyOnLoad(this.gameObject);
	}

	//Shows the message with the correct chatHeadTexture.
	public void ShowMessage(ChatMessage chatMessage) {
		_chatPanel.SetActive(true);
		_chatTexture.GetComponent<Image>().sprite = chatHeadTextures[chatMessage._chatTexture];
		_chatText.text = chatMessage._chatText;
		_chatActive = true;
	}

	public void ShowMessageInt(int category, int messageToShow) {
		//ChatMessage chatmessageTemp = new ChatMessage();
		//chatmessageTemp = chatMessageList[0].chatMessages[messageToShow];
		ShowMessage(chatMessageList[category].chatMessages[messageToShow]);
	}

	public void HideMessage() {
		_chatTexture.GetComponent<Image>().sprite = null;
		_chatText.text = "";
		_chatPanel.SetActive(false);
		_chatActive = false;
	}

	public void NextMessage() {
		//if there are no messages left.
		if(currentMessage >= chatMessageList[currentCategory].chatMessages.Count - 1) {
			if(currentCategory == 0) { //Menuscreen, dont show anything
				HideMessage();
			}
            //TODO keep this updated, bosslevel should probably be last, so include australia in here and all the middle parts if we have those.
			if(currentCategory == 1) { //Hanze
				currentMessage = 0;
				Debug.Log(currentCategory);
				SceneController.NextScene();
			}

            //for if messages run out but we still need to continue with the game
            //TODO probably dont need this
            if(currentCategory == 2 || currentCategory == 3)
            {
                currentMessage = 0;
            }

			currentCategory++;
			_chatActive = false;
			HideMessage();
			return;
		}

		currentMessage++;
		ShowMessageInt(currentCategory, currentMessage);
	}
}
