﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Health : MonoBehaviour {

	public float _health;

	public void Start() {
		if (tag == "Player") {
			GameObject.FindGameObjectWithTag("PlayerHealth").GetComponent<Text>().text = "health: " + _health.ToString();

		}
	}

	public void TakeDamage(float damage) {
		_health -= damage;

		if(tag == "Player") {
			GameObject.FindGameObjectWithTag("PlayerHealth").GetComponent<Text>().text = "health: " + _health.ToString();

		}

		if(_health <= 0) {
			if(tag == "Player") {
				//TODO show game over screen.
				Debug.Log("dead");
				SceneManager.LoadScene("GameOver");
			}else if(tag == "Spider" || tag == "Snake" || tag == "Kangeroo") {
				Destroy(gameObject);
                if(tag == "Kangeroo")
                {
                    SceneManager.LoadScene("WinScene");
                }
			}
		}
	}
}
