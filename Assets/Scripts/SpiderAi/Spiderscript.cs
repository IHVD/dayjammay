﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spiderscript : MonoBehaviour {

    public float speed;
    public float stoppingDistance;
    public float retreatDistance;

    private float timeBtwShots;
    public float startTimeBtwShots;
	public float _reloadSpeed;

    public GameObject projectile;
	public GameObject _webGun;
    public Transform player;

    private void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;

        timeBtwShots = startTimeBtwShots;
    }

    private void Update()
    {
		if(ChatSystem.CSystem._chatActive){
			return;
		}

        if (Vector2.Distance(transform.position, player.position) > stoppingDistance)
        {
            transform.position = Vector2.MoveTowards(transform.position, player.position, speed * Time.deltaTime);
        }
        else if (Vector2.Distance(transform.position, player.position) < stoppingDistance && Vector2.Distance(transform.position, player.position) > retreatDistance)
        {
            transform.position = this.transform.position;
        }
        else if (Vector2.Distance(transform.position, player.position) < retreatDistance)
            transform.position = Vector2.MoveTowards(transform.position, player.position, -speed * Time.deltaTime);

		//if(timeBtwShots >= 0 ){
		//
		//     Instantiate(projectile, transform.position, Quaternion.identity);
		//  }else {
		//
		//      timeBtwShots -= Time.deltaTime;
		//  }

		if (timeBtwShots <= 0) {
			Instantiate(projectile, _webGun.transform.position, Quaternion.identity);
			timeBtwShots = _reloadSpeed;
		} else {
			timeBtwShots -= Time.deltaTime;
		}
    }
           
    
}

