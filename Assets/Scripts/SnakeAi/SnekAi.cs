﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnekAi : MonoBehaviour
{

    public float speed;
    public float stoppingDistance;
    public float retreatDistance;


    public GameObject projectile;
    public Transform player;

    private void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;


    }

    private void Update()
    {
		if (ChatSystem.CSystem._chatActive) {
			return;
		}
		if (Vector2.Distance(transform.position, player.position) > stoppingDistance)
        {
            transform.position = Vector2.MoveTowards(transform.position, player.position, speed * Time.deltaTime);
        }
        else if (Vector2.Distance(transform.position, player.position) < stoppingDistance && Vector2.Distance(transform.position, player.position) > retreatDistance)
        {
            transform.position = this.transform.position;
        }
        else if (Vector2.Distance(transform.position, player.position) < retreatDistance)
            transform.position = Vector2.MoveTowards(transform.position, player.position, -speed * Time.deltaTime);
    }
}
