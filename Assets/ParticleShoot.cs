﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleShoot : MonoBehaviour {

	public float _projectileSpeed;
	public float _projectileLifeTime;

	public Rigidbody2D _rb2;

	// Update is called once per frame
	void Update () {
		if(_projectileLifeTime <= 0) {
			Destroy(gameObject);
		} else {
			transform.position = new Vector2(transform.position.x - _projectileSpeed * Time.deltaTime, transform.position.y);
			_projectileLifeTime -= Time.deltaTime;
		}
	}

	public void OnCollisionEnter2D(Collision2D collision) {
		if(collision.collider.tag == "Player") {
			collision.collider.GetComponent<Health>().TakeDamage(5f);
			Destroy(gameObject);
		}
	}
}
